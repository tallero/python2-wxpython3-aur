# Maintainer: Matthias Mailänder <matthias@mailaender.name>
# Contributor: dreieck
# Contributor: Eric Bélanger <eric@archlinux.org>
# Contributor: Pellegrino Prevete <pellegrinoprevete@gmail.com>

_py="python2"
_pkg="wxPython"
_pkgl="wxpython"
pkgname=("${_py}-${_pkgl}3")
_pkgver=3.0
pkgver="${_pkgver}.2.0"
pkgrel=11
pkgdesc="wxWidgets 3 Legacy GTK 3 GUI toolkit for Python 2."
depends=(
  "${_py}"
  'sdl'
)
provides=(
  "${_py}-${_pkgl}=${pkgver}"
  "${_py}-${_pkgl}-gtk3=${pkgver}"
  "${_py}-${_pkgl}3=${pkgver}"
  "${_pkgl}=${pkgver}"
  "${_pkgl}-gtk3=${pkgver}"
  "${_pkgl}3=${pkgver}"
  "${_pkgl}3-gtk3=${pkgver}"
  "wxwidgets3.0"
)
conflicts=(
  "${_pkgl}"
  "wxwidgets3.0" # ABI incompatible
)
replaces=("${_pkgl}")
arch=(
  "x86_64"
  "i686"
)
license=('custom:wxWindows')
_url="https://downloads.sourceforge.net"
url="https://www.${_pkgl}.org"

makedepends=(
  'glu'
  "${_py}-setuptools"
  'expat'
  'gstreamer0.10-base'
  'libjpeg'
  'libpng'
  'libtiff'
  "${_py}"
  'sdl')
#   'wxwidgets3.0'
# )

source=(
  "${_pkgl}-${pkgver}.tar.bz2::${_url}/${_pkgl}/${_pkg}-src-${pkgver}.tar.bz2"
  "fix-plot.patch"
  "fix-editra-removal.patch"
)
sha256sums=(
  'd54129e5fbea4fb8091c87b2980760b72c22a386cb3b9dd2eebc928ef5e8df61'
  '2d8b2bdbf55172738c7bf93955e552aac61842800c3b8b0a2e1f07f4314abc65'
  '559b076f206c44e66b33857f49068d66f59a3fc15b97972486e32a8105885554'
)


prepare() {
  cd "${srcdir}"
  rm -rf "${_pkgl}"
  mv "${_pkg}-src-${pkgver}" "${_pkgl}"
  msg2 "Patching shebang-lines for ${_py} ..."
  local _file
  local _grep_arg='(bin/env python$|bin/env python[[:space:]]|bin/python$|bin/python[[:space:]])'
  grep -Erl "${_grep_arg}" * | while read _file; do
    sed -Ei "s|bin/env python$|bin/env ${_py}|" "${_file}"
    sed -Ei "s|bin/env python([[:space:]])|bin/env ${_py}\1|" "${_file}"
    sed -Ei "s|bin/python$|bin/${_py}|" "${_file}"
    sed -Ei "s|bin/python([[:space:]])|bin/${_py}\1|" "${_file}"
  done
  ## The following python version fixing command was the original one.
  ## It is much slower, but kept here in case the above is not working correctly.
  # find . -type f -exec sed -i "s|env python|env ${_py}|" {} \;

  cd "${_pkgl}/${_pkg}"
  # Fix plot library (FS#42807)
  msg2 "Applying patch 'fix-plot.patch' ..."
  patch -Np1 -i ../../fix-plot.patch

  # Fix editra removal (FS#63563)
  msg2 "Applying patch  'fix-editra-removal.patch' ..."
  patch -Np2 -i ../../fix-editra-removal.patch
}

_wx_include="/usr/include/wx-${_pkgver}"
_gtk_unicode="lib/wx/config/gtk3-unicode-3.0"

_configure_opts=(
    --prefix='/usr'
    --libdir='/usr/lib'
    --includedir='/usr/include'
    --includedir="${_wx_include}"
    --disable-monolithic
    --disable-rpath
    --enable-geometry
    --enable-graphics_ctx
    --enable-optimise
    --enable-sound
    --enable-display
    --enable-unicode
    --with-gtk=3
    --with-python="/usr/bin/${_py}"
    --with-opengl
    --with-gnomeprint
    --with-sdl
)

# Old, non-maintained software. Use older C/C++ standard.
_C_std='gnu++14' # gnu11 # gnu17
_CXX_std='gnu++14' # gnu++11 # gnu++14 # gnu++17

# In case a newer C standard is used, warnings are generated which should not be treated as errors.
# Or just use -Wno-error to catch all.
_append_to_CFLAGS_after_configure=('-Wno-error=format-security'
                                   '-Wno-error=register'
                                   '-Wno-error=deprecated-declarations'
                                   '-Wno-error=alloc-size-larger-than='
                                   '-Wno-error=write-strings'
                                   '-Wno-error=return-local-addr'
                                   '-Wno-error=attributes')
# Or just use -Wno-error to catch all.
# _append_to_CFLAGS_after_configure='-Wno-error'
_append_to_CXXFLAGS_after_configure="${_append_to_CFLAGS_after_configure[*]}"

build() {
  cd "${srcdir}/${_pkgl}" || exit
  CFLAGS+=" -std=${_C_std} -I${_wx_include}"
  CXXFLAGS+=" -std=${_CXX_std} -I${_wx_include}"
  export CFLAGS
  export CXXFLAGS

  ./configure "${_configure_opts[@]}"

  CFLAGS+=" ${_append_to_CFLAGS_after_configure[*]} -std=${_C_std}"
  CXXFLAGS+=" ${_append_to_CXXFLAGS_after_configure} -std=${_CXX_std}"
  export CFLAGS
  export CXXFLAGS

  make

  cd "${_pkg}"
  local _wx_config=("${srcdir}/${_pkgl}/${_gtk_unicode}"
                    "--prefix=/usr"
                    "--no_rpath")
  local _sys_wx_config=("/usr/${_gtk_unicode}"
                        "--version=${pkgver}"
                        "--toolkit=gtk3"
                        "--static=no")
  wxUSE_WEBVIEW="no" \
  USE_WEBVIEW=0 \
  "${_py}" setup.py WXPORT=gtk3 \
                    UNICODE=1 \
                    EP_ADD_OPTS=1 \
                    EP_FULL_VER=0 \
                    NO_SCRIPTS=1 \
                    WX_CONFIG="${_wx_config[*]}" \
                    SYS_WX_CONFIG="${_sys_wx_config[*]}" \
                    build_ext --rpath=/usr/lib \
                              build
}

package() {
  cd "${srcdir}/${_pkgl}" || exit

  make prefix=/usr install DESTDIR="${pkgdir}"

  cd "${_pkg}" || exit
  local _wx_config=("${srcdir}/${_pkgl}/${_gtk_unicode}"
                    "--prefix=/usr"
                    "--no_rpath")
  local _sys_wx_config=("/usr/${_gtk_unicode}"
                        "--version=${pkgver}"
                        "--toolkit=gtk3"
                        "--static=no")
  "${_py}" setup.py WXPORT=gtk3 \
                    UNICODE=1 \
                    EP_ADD_OPTS=1 \
                    EP_FULL_VER=0 \
                    NO_SCRIPTS=1 \
                    WX_CONFIG="${_wx_config[*]}" \
                    SYS_WX_CONFIG="${_sys_wx_config[*]}" \
                    build_ext --rpath=/usr/lib \
                              install --root="${pkgdir}" \
                                      --prefix=/usr

  # Avoid conflicts
  rm "${pkgdir}/usr/bin" -fr
  rm "${pkgdir}/usr/share" -fr

  install -Dvm644 '../docs/licence.txt' "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
}
